#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Cai Xiaoxuan, Duan Shiqiang"
__copyright__ = "@COPYLEFT ALL WRONGS RESERVED"
__license__ = "GPLv3"
__version__ = "1.0.0"

import threading
import ConfigParser
import os
import sys
import struct
import logging.config
import logging
import socket
import time
import serial
from Queue import Queue
from pyudev import Context, Monitor
import bluetooth

configFilePath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini')
config = ConfigParser.ConfigParser()
config.read(configFilePath)
loggingconfidict = {
    'version': 1, 
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
            'datefmt': '%Y%m%d %H:%M:%S',
        },
    },
    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'level': 'INFO',
            'formatter': 'standard',
            'filename': os.path.expanduser( config.get('log', 'path') ),
            'encoding': 'utf-8',
            'mode': 'w',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout',
        },
    },
    'loggers': {
        __name__: {
            'level': 'INFO',
            'handlers': ('file', 'console'),
        },
    },
}
logging.config.dictConfig(loggingconfidict)
logger = logging.getLogger(__name__)

bluetooth_connected = False
algorithm2bt_connected = False
bt_client_sock = None
alg2bt_client_sock = None

arduino_connected = False
algorithm2ardu_connected = False
serial_inst = None
alg2ardu_client_sock = None

def mygetData(sock, length):
    data = ''
    while len(data)<length:
        chunk = sock.recv(length-len(data))
        if chunk == '':
            raise RuntimeError("socket connection broken")
        data = data + chunk
    return data

class bt_recv_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
            
    def init(self):
        global bt_client_sock, bluetooth_connected
        self.bt_server_sock = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        self.bt_server_sock.bind( ("", int(config.get('bluetooth', 'port'))) )
        self.bt_server_sock.listen(1)
        uuid = config.get('bluetooth', 'UUID' )
        
        bluetooth.advertise_service(self.bt_server_sock, "MDPGrp2",
                                    service_id=uuid,
                                    service_classes=[ uuid, bluetooth.SERIAL_PORT_CLASS ],
                                    profiles = [ bluetooth.SERIAL_PORT_PROFILE ] )
        (bt_client_sock, self.addrport) = self.bt_server_sock.accept()
        logger.info('Bluetooth accepted connection from ' + str(self.addrport))
        bluetooth_connected = True
    
    def reinit(self):
        global bt_client_sock, bluetooth_connected
        bluetooth_connected = False
        bt_client_sock.close()
        self.bt_server_sock.close()
        self.init()
    
    def run(self):
        self.init()
        while True:
            try:
                data_header = mygetData(bt_client_sock, 5)
            except Exception as e:
                logger.error("Bluetooth recv data exception, error message: %s", e)
                logger.info("Then reinitialize bluetooth server socket.")
                self.reinit()
                continue
            
            if data_header[4] != '\xAA':
                logger.warn("receved corrupted bluetooth data.")
                logger.info("Then reinitialize bluetooth server socket.")
                self.reinit()
                continue
            data_length = struct.unpack('<i', data_header[:4])[0]
            try:
                data = mygetData(bt_client_sock, data_length)
            except Exception as e:
                logger.error("Bluetooth recv data exception, error message: %s", e)
                logger.info("Then reinitialize bluetooth server socket.")
                self.reinit()
                continue
            # Finally we got the data
            if algorithm2bt_connected:
                self.data_queue.put(struct.pack('<i', len(data)) + '\xAA' + data)
            else:
                logger.info("Algorithm not connected, so discard data from Bluetooth: "+data)


class bt_send_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
    
    def run(self):
        while True:
            data = self.data_queue.get()
            try:
                if bluetooth_connected: # this may useless
                    bt_client_sock.sendall(data)
            except Exception:
                logger.error("Bluetooth socket send out data error. Sorry I was not able to handle this.")
            finally:
                self.data_queue.task_done()        


class algo2bt_recv_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
    
    def init(self):
        global algorithm2bt_connected, alg2bt_client_sock
        self.serversocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.bind( ('0.0.0.0', int(config.get('algo-bt', 'port'))) )
        self.serversocket.listen(1)
        (alg2bt_client_sock, self.client_address) = self.serversocket.accept()
        logger.info('Algoritm-Bluetooth accepted connection from ' + str(self.client_address))
        algorithm2bt_connected = True
    
    def reinit(self):
        global algorithm2bt_connected, alg2bt_client_sock
        algorithm2bt_connected = False
        alg2bt_client_sock.close()
        self.serversocket.close()
        self.init()
    
    def run(self):
        self.init()
        while True:
            try:
                data_header = mygetData(alg2bt_client_sock, 5)
            except Exception as e:
                logger.error("Algoritm-Bluetooth recv data exception, error message: %s", e)
                logger.info("Then reinitialize Algoritm-Bluetooth server socket.")
                self.reinit()
                continue
            
            if data_header[4] != '\xAA':
                logger.warn("receved corrupted Algoritm-Bluetooth data.")
                logger.info("Then reinitialize Algoritm-Bluetooth server socket.")
                self.reinit()
                continue
            data_length = struct.unpack('<i', data_header[:4])[0]
            try:
                data = mygetData(alg2bt_client_sock, data_length)
            except Exception as e:
                logger.error("Algoritm-Bluetooth recv data exception, error message: %s", e)
                logger.info("Then reinitialize Algoritm-Bluetooth server socket.")
                self.reinit()
                continue
            # Finally we got the data
            if bluetooth_connected:
                self.data_queue.put(struct.pack('<i', len(data)) + '\xAA' + data)
            else:
                logger.info("Bluetooth not connected, so discard data from Algorithm: "+data)


class algo2bt_send_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
    
    def run(self):
        while True:
            data = self.data_queue.get()
            try:
                if algorithm2bt_connected: # this may useless
                    alg2bt_client_sock.sendall(data)
            except Exception:
                logger.error("Algoritm-Bluetooth socket send out data error. Sorry I was not able to handle this.")
            finally:
                self.data_queue.task_done()


class ardu_recv_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
        self.monitor = Monitor.from_netlink(Context())
        self.monitor.start()
        self.monitor.filter_by('tty')
        
    def init(self, arduPath):
        global serial_inst, arduino_connected
        serial_inst = serial.Serial(arduPath, int(config.get('arduino', 'baudrate')))
        serial_inst.flushInput()
        serial_inst.flushOutput()
        logger.info('Connection between Raspberry Pi and Arduino is established!')
        arduino_connected = True
    
    
    def pollUntilUSBConnected(self):
        arduino_vendor = config.get('arduino', 'serialvendor')
        while(True):
            device = self.monitor.poll()
            if device.action != u"add":
                continue
            if device['ID_VENDOR'] == arduino_vendor:
                return device.device_node
                
    def reinit(self):
        global serial_inst, arduino_connected
        arduino_connected = False
        serial_inst.close()
        path = self.pollUntilUSBConnected()
        self.init(path)
        
    def run(self):
        initpaths = [path for path in config.get('arduino', 'path').split('|') if os.path.exists(path)]
        if initpaths:
            initpath = initpaths[0]
        else:
            initpath = self.pollUntilUSBConnected()
        self.init(initpath)
        while True:
            try:
                data = serial_inst.read()
            except Exception as e:
                logger.error("Arduino receive data exception, error message: %s", e)
                logger.info("Then reinitialize serial connection")
                self.reinit()    
                continue
            
            if algorithm2ardu_connected:
                self.data_queue.put(data)
            else:
                logger.info("Algorithm not connected, so discard data from Arduino: "+data)


class ardu_send_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
        
    def run(self):
        while True:
            data = self.data_queue.get()
            try:
                if arduino_connected:
                    serial_inst.write(data)
            except Exception:
                logger.error("Arduino serial send out data error. Sorry I was not able to handle this.")
            finally:
                self.data_queue.task_done()


class algo2ardu_recv_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
    
    def init(self):
        global algorithm2ardu_connected, alg2ardu_client_sock
        self.serversocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.bind( ('0.0.0.0', int(config.get('algo-arduino', 'port'))) )
        self.serversocket.listen(1)
        (alg2ardu_client_sock, self.client_address) = self.serversocket.accept()
        logger.info('Algoritm-Arduino accepted connection from ' + str(self.client_address))
        algorithm2ardu_connected = True
    
    def reinit(self):
        global algorithm2ardu_connected, alg2ardu_client_sock
        algorithm2ardu_connected = False
        alg2ardu_client_sock.close()
        self.serversocket.close()
        self.init()
    
    def run(self):
        self.init()
        while True:
            try:
                data = mygetData(alg2ardu_client_sock, 1)
            except Exception as e:
                logger.error("Algoritm-Arduino recv data exception, error message: %s", e)
                logger.info("Then reinitialize Algoritm-Arduino server socket.")
                self.reinit()
                continue
            
            if arduino_connected:
                self.data_queue.put(data)
            else:
                logger.info("Arduino not connected, so discard data from Algorithm: "+data)


class algo2ardu_send_thread(threading.Thread):
    def __init__(self, data_queue, group=None, target=None, name=None, args=(), kwargs={}):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
        self.data_queue = data_queue
    
    def run(self):
        while True:
            data = self.data_queue.get()
            try:
                if algorithm2ardu_connected: # this may useless
                    alg2ardu_client_sock.sendall(data)
            except Exception:
                logger.error("Algoritm-Arduino socket send out data error. Sorry I was not able to handle this.")
            finally:
                self.data_queue.task_done()


if __name__ == "__main__":
    algo2bt_queue = Queue()
    bt2algo_queue = Queue()
    algo2ardu_queue = Queue()
    ardu2algo_queue = Queue()
    t1 = bt_recv_thread(bt2algo_queue, name='bt_recv_thread')
    t2 = algo2bt_recv_thread(algo2bt_queue, name='algo2bt_recv_thread')
    t3 = bt_send_thread(algo2bt_queue, name='bt_send_thread')
    t4 = algo2bt_send_thread(bt2algo_queue, name='algo2bt_send_thread')
    t5 = ardu_recv_thread(ardu2algo_queue, name='ardu_recv_thread')
    t6 = ardu_send_thread(algo2ardu_queue, name='ardu_send_thread')
    t7 = algo2ardu_recv_thread(algo2ardu_queue, name='algo2ardu_recv_thread')
    t8 = algo2ardu_send_thread(ardu2algo_queue, name='algo2ardu_send_thread')
    for thread in [t1, t2, t3, t4, t5, t6, t7, t8]:
        thread.daemon = True
        thread.start()
    logger.info('Created all threads, service is up, commander!')
    for thread in [t1, t2, t3, t4, t5, t6, t7, t8]:
        thread.join()
